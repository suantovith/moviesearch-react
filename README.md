# MovieSearch
## Next.JS + NextUI React SPA
### Rewrite of [Vue MovieSearch](https://gitlab.com/suantovith/movie-search)

TODO:
 - Autocomplete for searchbox
 - Filter movies by cast, release date, genre and name

**You can deploy MovieSearch with [Docker](https://www.docker.com/).**
```sh
docker-compose up -d
```
> After deploy just open [http://localhost:3000](http://localhost:3000) in your browser.


**MovieSearch requires [Yarn](https://yarnpkg.com/) to develop.**

Install the dependencies and packages:
```sh
yarn
```

To develop app use:
```sh
yarn dev
```
  
To build packages use:
```sh
yarn build
```

To start it only use:
```sh
yarn start
```

To run tests use:
```sh
yarn test
```

To check code use:
```sh
yarn lint
```