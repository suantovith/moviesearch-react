import { type Movie, type MovieListProps } from "@/lib/types";
import { Card, Grid, Text, Image, } from "@nextui-org/react";
import { type NextRouter, useRouter } from "next/router";

const MovieList = ({ list }: MovieListProps): JSX.Element => {
    const router: NextRouter = useRouter();
    return (<>
        {list.length === 0 && (<Text h3>Sorry, nothing to show here...</Text>)}
        <Grid.Container data-testid="movie-list" justify="flex-start" gap={2}>
            {list.map((e: Movie, i: number) => <Grid key={i} xs={12} sm={6}>
                <Card onPress={() => { router.push(`/movie/${e.id}`) }} isPressable isHoverable css={{
                    p: "$6",
                    mw: "650px",
                    "@smMax": {
                        mw: "100%",
                    },
                }}>
                    <Card.Body>
                        <Grid.Container gap={1} css={{ pl: "$6" }}>
                            <Grid xs={12} sm={4}>
                                <Image
                                    alt={e.title}
                                    src={`https://image.tmdb.org/t/p/original/${e.poster_path}`}
                                    objectFit="cover"
                                />
                            </Grid>
                            <Grid xs={12} sm={8}>
                                <div>
                                    <Text h4 css={{ pb: 0, mb: 0 }}>
                                        {e.title}
                                    </Text>
                                    <Text size={12} css={{ color: "grey", minHeight: "1em" }} weight="light">
                                        {e.title === e.original_title ? "" : e.original_title}
                                    </Text>
                                    <Text size={14}>{e.overview}</Text>
                                </div>
                            </Grid>
                        </Grid.Container>
                    </Card.Body>
                </Card>

            </Grid>)}
        </Grid.Container>

    </>);
}
export default MovieList;