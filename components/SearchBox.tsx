import { KeyboardEventHandler, KeyboardEvent, useState, ChangeEvent } from "react";
import { FormElement, Input } from "@nextui-org/react";
import { NextRouter, useRouter } from "next/router";
import { FaSearch } from "react-icons/fa";

const SearchBox = (): JSX.Element => {
    const router: NextRouter = useRouter();
    const [search, setSearch] = useState("");
    const handleKeyDown: KeyboardEventHandler = (e: KeyboardEvent<HTMLInputElement>) => {
        if ((e.key === "Enter" || e.key === "Return") && search !== "") {
            router.push({
                pathname: "/search",
                query: { q: search }
            });
        }
    }
    return (
        <>
            <Input
                onChange={(e: ChangeEvent<FormElement>) => setSearch(e.target.value)}
                onKeyDown={handleKeyDown}
                data-testid="searchbox"
                aria-label="Search..."
                contentLeft={
                    <FaSearch />
                }
                contentLeftStyling={false}
                css={{
                    w: "300px",
                    mw: "100%",
                    "@xsMax": {
                        mw: "100px",
                    },
                    "& .nextui-input-content--left": {
                        h: "100%",
                        ml: "$4",
                        dflex: "center",
                    },
                }} clearable placeholder="Search for a movie, tv show, person......" />
        </>
    );
}

export default SearchBox;