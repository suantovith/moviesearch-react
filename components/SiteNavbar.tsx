import { Button, Navbar, Text } from "@nextui-org/react";
import Link from "next/link";
import { FaHome, FaStar } from "react-icons/fa";
import ThemeSwitcher from "./ThemeSwitcher";
import SearchBox from "./SearchBox";

const SiteNavbar = (): JSX.Element => {
  return (
    <Navbar shouldHideOnScroll isCompact variant="sticky">
      <Navbar.Brand>
        <Button as={Link} href="/" auto css={{ padding: "$4" }} light>
          <FaHome size="2em" />
        </Button>
      </Navbar.Brand>
      <Navbar.Content>
        <Navbar.Item>
          <SearchBox />
        </Navbar.Item>
        <Navbar.Item>
          <Button auto icon={<FaStar />} as={Link} href="/favorites" color="success" flat>
            <Text size={18}>
              Favorites
            </Text>
          </Button>
        </Navbar.Item>
        <Navbar.Item
          css={{
            "@xsMax": {
              w: "100%",
              jc: "center",
            },
          }}>
          <ThemeSwitcher />
        </Navbar.Item>
      </Navbar.Content>
    </Navbar>
  );
};
export default SiteNavbar;