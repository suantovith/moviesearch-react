import { useTheme as useNextTheme } from "next-themes"
import { Switch, SwitchEvent, useTheme } from "@nextui-org/react"
import { FaMoon, FaSun } from "react-icons/fa";

const ThemeSwitcher = (): JSX.Element => {
  const { setTheme } = useNextTheme();
  const isDark: boolean = useTheme().isDark ?? false;

  return (
    <>
      <Switch
        data-testid="theme-switch"
        checked={isDark}
        iconOn={<FaMoon />}
        iconOff={<FaSun />}
        onChange={(e: SwitchEvent) => setTheme(e.target.checked ? "dark" : "light")}
      />
    </>
  )
}

export default ThemeSwitcher;