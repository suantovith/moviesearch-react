/** @type {import("ts-jest").JestConfigWithTsJest} */
module.exports = {
  moduleNameMapper: {"^@/(.*)$": "<rootDir>/$1"},
  setupFiles: ["dotenv/config"],
  preset: "ts-jest",
  testEnvironment: "node",
  transform: {
    "^.+\\.tsx?$": ["ts-jest", {
      tsconfig: {
        jsx: "react-jsx"
      }
    }]
  }
};