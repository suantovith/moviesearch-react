import { type LayoutProps } from "@/lib/types";
import SiteNavbar from "../components/SiteNavbar";
import { Container } from "@nextui-org/react";
import { Providers } from "@/lib/providers"

const Layout = ({ children }: LayoutProps): JSX.Element => {
    return (
        <Providers>
            <SiteNavbar />
            <main data-testid="main">
                <Container lg css={{ paddingTop: "$12" }}>
                    {children}
                </Container>
            </main>
        </Providers>
    );
}
export default Layout;