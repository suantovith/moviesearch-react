const prepareString = (string: string): string  => {
    if (string == undefined || string == null) {
        return "";
    }
    return escape(string.normalize("NFD").replace(/[\u0300-\u036f]/g, "").replace(/\u0142/g, "l"));
}

export default prepareString;