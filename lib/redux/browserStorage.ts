import { type ReduxState } from "../types";

const key: string = process.env.NEXT_PUBLIC_APP_ALIAS ?? 'store';
export const loadState = (): ReduxState | undefined => {
  try {
    const serializedState: string | null = localStorage.getItem(key);
    if (!serializedState) return undefined;
    return JSON.parse(serializedState);
  } catch (error) {
    //Intentional behavior
    //console.error(error);
    return undefined;
  }
};

export const saveState = async (state: ReduxState): Promise<void> => {
  try {
    const serializedState: string = JSON.stringify(state);
    localStorage.setItem(key, serializedState);
  } catch (error) {
    //Intentional behavior
    //console.error(error);
  }
};