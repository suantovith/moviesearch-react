import { type Middleware } from "@reduxjs/toolkit"
import { createLogger } from "redux-logger"

const middleware: Middleware[] = [
    createLogger({
        duration: true,
        timestamp: false,
        collapsed: true,
        colors: {
            title: () => "#139BFE",
            prevState: () => "#1C5FAF",
            action: () => "#149945",
            nextState: () => "#A47104",
            error: () => "#ff0005",
        },
        predicate: (): boolean => typeof window !== "undefined",
    }),
]

export { middleware }