import { favoritesSlice } from "./slices"

export const reducer = {
    favorites: favoritesSlice.reducer
}