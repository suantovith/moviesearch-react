import { type FavoritesSliceState, type Movie } from "@/lib/types"
import { type PayloadAction, type Slice, createSlice } from "@reduxjs/toolkit"

const initialState: FavoritesSliceState = {
    list: []
};

export const favoritesSlice: Slice<any> = createSlice({
    name: "favorites",
    initialState,
    reducers: {
        add: (state: FavoritesSliceState, action: PayloadAction<Movie>) => {
            const duplicate: boolean = Boolean(state.list.filter((e: Movie) => {
                return e.id === action.payload.id;
            }).length);
            if (!duplicate) {
                state.list = [...state.list, action.payload];
            }
            return state;
        },
        remove: (state: FavoritesSliceState, action: PayloadAction<number>) => {
            state.list = state.list.filter((e: Movie) => {
                return e.id !== action.payload;
            });
            return state;
        }
    }
});