import { type ReduxState, type Movie } from "@/lib/types";

export const selectFavorites = (state: ReduxState): Movie[] => state.favorites.list;