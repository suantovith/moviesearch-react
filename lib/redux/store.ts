import { type ReduxMiddleware } from "../types";
import {
    configureStore,
    type ConfigureStoreOptions
} from "@reduxjs/toolkit"
import { type ToolkitStore } from "@reduxjs/toolkit/dist/configureStore";
import { type CurriedGetDefaultMiddleware } from "@reduxjs/toolkit/dist/getDefaultMiddleware";
import { saveState, loadState } from "./browserStorage";
import { reducer } from "./rootReducer"
import { middleware } from "./middleware"

const configreStoreDefaultOptions: ConfigureStoreOptions = { reducer }

export const makeReduxStore = (
    options: ConfigureStoreOptions = configreStoreDefaultOptions
) => {
    const store = configureStore(options);
    return store;
};

export const reduxStore: ToolkitStore = configureStore({
    reducer,
    middleware: (getDefaultMiddleware: CurriedGetDefaultMiddleware): ReduxMiddleware => {
        return getDefaultMiddleware().concat(middleware)
    },
    preloadedState: loadState()
});

reduxStore.subscribe(
    (): void => {
        saveState(reduxStore.getState());
    }
);