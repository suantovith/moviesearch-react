
export interface Movie {
    id: number;
    title: string;
    overview: string;
    poster_path: string;
    original_title?: string;
    vote_average?: string;
};
export interface MovieListResponse {
    results: Movie[];
};
export interface MovieListProps {
    list: Movie[];
};
export interface HomeProps extends MovieListProps { };
export interface MovieProps {
    item: Movie;
};
export interface SearchProps {
    list: Movie[];
    search: string;
};
export interface Favorites extends Array<Movie> { };
export interface LayoutProps {
    children: JSX.Element[] | JSX.Element;
};
export interface FavoritesSliceState {
    list: Favorites
};
export type ReduxState = any;
export type ReduxMiddleware = any;