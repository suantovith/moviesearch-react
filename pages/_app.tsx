import { type AppProps } from "next/app";
import { createTheme, NextUIProvider, useSSR, type Theme } from "@nextui-org/react";
import { ThemeProvider as NextThemesProvider } from "next-themes";
import Layout from "../layouts/Default";

const lightTheme: Theme = createTheme({
  type: "light",
});
const darkTheme: Theme = createTheme({
  type: "dark",
});

const App = ({ Component, pageProps }: AppProps): boolean | JSX.Element => {
  const isBrowser: boolean = useSSR().isBrowser;
  return (
    isBrowser && (
      <NextThemesProvider
        defaultTheme="system"
        attribute="class"
        value={{
          light: lightTheme.className ?? "",
          dark: darkTheme.className ?? "",
        }}
      >
        <NextUIProvider>
          <Layout>
            <Component {...pageProps} />
          </Layout>
        </NextUIProvider>
      </NextThemesProvider>
    )
  );
};

export default App;
