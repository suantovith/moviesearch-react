
import { type MovieListResponse, type Movie } from "@/lib/types";
import { type NextApiHandler, type NextApiRequest, type NextApiResponse } from "next";

const getPopulars = async (): Promise<Movie[]> => {
  const response: Response = await fetch(
    `${process.env.API_URL}/movie/popular?api_key=${process.env.API_KEY}`
  );
  const data: MovieListResponse = await response.json();
  return data.results;
};

const handler: NextApiHandler = async (req: NextApiRequest, res: NextApiResponse): Promise<any> => {
  const data: Movie[] = await getPopulars();
  res.status(200).json(data);
};

export { getPopulars };
export default handler;
