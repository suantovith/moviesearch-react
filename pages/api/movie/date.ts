import { type Movie, type MovieListResponse } from "@/lib/types";
import { type NextApiRequest, type NextApiResponse } from "next";

const getMoviesByDate = async (date: string, mode: string): Promise<Movie[]> => {
  const dateObj: Date = new Date(date);
  const newDate: string = `${dateObj.getUTCFullYear()}-${(
    "0" +
    (dateObj.getUTCMonth() + 1)
  ).slice(-2)}-${("0" + dateObj.getUTCDate()).slice(-2)}`;

  const response: Response = await fetch(
    `${process.env.API_URL}/discover/movie?release_date.${mode == "gte" ? "gte" : "lte"
    }=${newDate}&api_key=${process.env.API_KEY}`
  );
  const data: MovieListResponse = await response.json();
  return data.results;
};

const handler = async (req: NextApiRequest, res: NextApiResponse): Promise<any> => {
  if (typeof req.query.date === "string" && typeof req.query.mode === "string") {
    const data: Movie[] = await getMoviesByDate(req.query.date, req.query.mode);
    return res.status(200).json(data);
  }
  return res.status(404);
};

export { getMoviesByDate };
export default handler;
