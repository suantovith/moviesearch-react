import { type Movie, type MovieListResponse } from "@/lib/types";
import { type NextApiRequest, type NextApiResponse } from "next";
import prepareString from "@/lib/prepareString";

const getMoviesByName = async (string: string): Promise<Movie[]> => {
  const response: Response = await fetch(
    `${process.env.API_URL}/search/movie?query=${prepareString(
      string
    )}&api_key=${process.env.API_KEY}`
  );
  const data: MovieListResponse = await response.json();
  return data.results;
};

const handler = async (req: NextApiRequest, res: NextApiResponse): Promise<any> => {
  if(typeof req.query.string === "string"){
    const data: Movie[] = await getMoviesByName(req.query.string);
    return res.status(200).json(data);
  }
  return res.status(404);
};

export { getMoviesByName };
export default handler;
