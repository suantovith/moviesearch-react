import { type Movie, type MovieListResponse } from "@/lib/types";
import { type NextApiRequest, type NextApiResponse } from "next";

const getMoviesByPerson = async (id: number): Promise<Movie[]> => {
  const response: Response = await fetch(
    `${process.env.API_URL}/discover/movie?with_people=${id}&api_key=${process.env.API_KEY}`
  );
  const data: MovieListResponse = await response.json();
  return data.results;
};

const handler = async (req: NextApiRequest, res: NextApiResponse): Promise<any> => {
  if (typeof req.query.id === "string") {
    const id: number = parseInt(req.query?.id);
    const data: Movie[] = await getMoviesByPerson(id);
    return res.status(200).json(data);
  }
  return res.status(404);
};

export { getMoviesByPerson };
export default handler;
