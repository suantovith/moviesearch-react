import { type Movie } from "@/lib/types";
import { type NextApiRequest, type NextApiResponse } from "next";

const getMovieById = async (id: number): Promise<Movie> => {
  const response: Response = await fetch(
    `${process.env.API_URL}/movie/${id}?api_key=${process.env.API_KEY}`
  );
  const data: Movie = await response.json();
  return data;
};

const handler = async (req: NextApiRequest, res: NextApiResponse): Promise<any> => {
  if (typeof req.query.id === "string") {
    const id: number = parseInt(req.query?.id);
    const data: Movie = await getMovieById(id);
    return res.status(200).json(data);
  }
  return res.status(404);
};

export { getMovieById };
export default handler;