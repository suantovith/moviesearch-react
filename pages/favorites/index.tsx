import { type Favorites } from "@/lib/types";
import { useSelector } from "react-redux";
import { selectFavorites } from "@/lib/redux";
import MovieList from "@/components/MovieList";

const Favs = (): JSX.Element => {
  document.title = "Favorites";
  const favorites: Favorites = useSelector(selectFavorites);
  return (
    <>
      <MovieList list={favorites} />
    </>
  );
};

export default Favs;
