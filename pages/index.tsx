import { type HomeProps, type Movie } from "@/lib/types";
import { type NextRouter, useRouter } from "next/router";
import Head from "next/head";
import { Card, Container, Text, Grid, Row } from "@nextui-org/react";
import { GetServerSideProps } from "next";
import { getPopulars } from "./api/home";



export const getServerSideProps: GetServerSideProps = async (): Promise<any> => {
  try {
    const list: Movie[] = await getPopulars();
    return {
      props: { list }
    };
  } catch (error) {
    console.error(error);
    return {
      props: { list: [] },
    };
  }
};

const Home = ({ list }: HomeProps): JSX.Element => {
  const router: NextRouter = useRouter();
  return (
    <>
      <Head>
        <title>MovieSearch</title>
      </Head>
      <main>
        <header data-testid="hero">
          <Container md>
            <Card variant="bordered">
              <Card.Body css={{ padding: "$24" }}>
                <Text
                  h1
                  css={{ textGradient: "45deg, $blue600 -20%, $pink600 50%" }}
                >
                  Welcome to MovieSearch!
                </Text>
                <Text size={24}>
                  Search for your favorite movie by date, genre, cast or just
                  name...
                </Text>
              </Card.Body>
            </Card>
          </Container>
        </header>
        <Text h2 css={{ mb: 0, mt: "1em" }}>
          Trending right now:
        </Text>
        <Grid.Container data-testid="populars" gap={2} justify="flex-start">
          {list.map((e: Movie, i: number) => (
            <Grid xs={6} sm={2} key={i}>
              <Card
                onPress={() => {
                  router.push(`/movie/${e.id}`);
                }}
                isPressable
                isHoverable
              >
                <Card.Body css={{ p: 0 }}>
                  <Card.Image
                    src={`https://image.tmdb.org/t/p/original/${e.poster_path}`}
                    objectFit="cover"
                    width="100%"
                    height={240}
                    alt={e.title}
                  />
                </Card.Body>
                <Card.Footer
                  css={{
                    position: "absolute",
                    bgBlur: "#ffffff66",
                    borderTop:
                      "$borderWeights$light solid rgba(255, 255, 255, 0.2)",
                    bottom: 0,
                    zIndex: 1,
                  }}
                  isBlurred
                >
                  <Row wrap="wrap" justify="space-between" align="center">
                    <Text size={12} b>
                      {e.title}
                    </Text>
                  </Row>
                </Card.Footer>
              </Card>
            </Grid>
          ))}
        </Grid.Container>
      </main>
    </>
  );
};
export default Home;