import { type MovieProps, type Movie, type Favorites } from "@/lib/types";
import { type GetServerSideProps, type GetServerSidePropsContext } from "next";
import { Card, Grid, Text, Image, Spacer, Button } from "@nextui-org/react";
import { FaMinus, FaPlus } from "react-icons/fa";
import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Dispatch } from "@reduxjs/toolkit";
import { favoritesSlice, selectFavorites } from "@/lib/redux";
import { getMovieById } from "../api/movie/single";

export const getServerSideProps: GetServerSideProps = async (context: GetServerSidePropsContext): Promise<any> => {
  if (typeof context?.query?.slug === "string") {
    const id: number = parseInt(context?.query?.slug);
    const item: Movie = await getMovieById(id);
    return { props: { item: item } };
  }
  return { props: {} };
};

const Movie = (props: MovieProps): JSX.Element => {
  const favorites: Favorites = useSelector(selectFavorites);
  const dispatch: Dispatch = useDispatch()
  const item: Movie = props.item;
  document.title = item?.title;

  const isFavorite = (): boolean => {
    const count: number = favorites.filter((e: Movie) => {
      return e.id === item.id;
    }).length;
    if (count === 0) {
      return false;
    }
    return true;
  };

  const [favorite, setFavorite] = useState(isFavorite());

  const addToFavorites = (): boolean => {
    dispatch(favoritesSlice.actions.add(item));
    setFavorite(true);
    return true;
  };
  const removeFromFavorites = (): boolean => {
    dispatch(favoritesSlice.actions.remove(item.id));
    setFavorite(false);
    return true;
  };

  return (
    <>
      <main>
        <Card
          css={{
            p: "$6",
            mw: "100%",
            "@smMax": {
              mw: "100%",
            },
          }}
        >
          <Card.Body>
            <Grid.Container gap={2} css={{ pl: "$6" }}>
              <Grid xs={12} sm={4}>
                <Image
                  data-testid="poster"
                  alt={item?.title}
                  src={`https://image.tmdb.org/t/p/original/${item?.poster_path}`}
                  objectFit="cover"
                />
              </Grid>
              <Grid xs={12} sm={8}>
                <div>
                  <Text h2 css={{ pb: 0, mb: 0 }}>
                    {item?.title}
                  </Text>
                  <Text
                    h3
                    css={{ color: "grey", minHeight: "1em" }}
                    weight="light"
                  >
                    {item?.title === item?.original_title
                      ? ""
                      : item?.original_title}
                  </Text>
                  <Text size={18}>{item?.overview}</Text>
                  <Spacer />
                  <Text css={{ color: "grey" }}>
                    Average rating: {item?.vote_average}
                  </Text>
                  <Spacer />
                  {!favorite && (
                    <Button
                      auto
                      icon={<FaPlus />}
                      color="success"
                      flat
                      onClick={addToFavorites}
                    >
                      Add to favorites
                    </Button>
                  )}
                  {favorite && (
                    <Button
                      auto
                      icon={<FaMinus />}
                      color="error"
                      flat
                      onClick={removeFromFavorites}
                    >
                      Remove from favorites
                    </Button>
                  )}
                </div>
              </Grid>
            </Grid.Container>
          </Card.Body>
        </Card>
      </main>
    </>
  );
};
export default Movie;
