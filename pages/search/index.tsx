import { type Movie, type SearchProps } from "@/lib/types";
import { type GetServerSideProps, type GetServerSidePropsContext } from "next";
import MovieList from "@/components/MovieList";
import { getMoviesByName } from "../api/movie";

export const getServerSideProps: GetServerSideProps = async (context: GetServerSidePropsContext): Promise<any> => {
  if (typeof context?.query?.q === "string") {
    const search: string = context?.query?.q ?? "";
    const list: Movie[] = await getMoviesByName(search);
    return { props: { search: search, list: list } };
  }
  return { props: {} };
};

const Search = ({ search, list }: SearchProps): JSX.Element => {
  document.title = "Search for a movie...";
  return (
    <>
      <MovieList list={list} />
    </>
  );
};

export default Search;
