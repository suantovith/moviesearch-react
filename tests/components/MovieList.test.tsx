/**
 * @jest-environment jsdom
 */

import { type Movie } from "@/lib/types";
import "@jest/globals";
import "@testing-library/jest-dom";
import { render, screen } from "@testing-library/react";
import { MemoryRouterProvider } from "next-router-mock/MemoryRouterProvider";
import MovieList from "@/components/MovieList";

describe("Renders movies list component", () => {
    it("should render movies container", () => {
        const movie: Movie = {
            id: 0,
            title: "Test",
            original_title: "_TEST_",
            overview: "TEST TEST TEST",
            poster_path: ""
        }
        render(<MovieList list={[movie]} />, {
            wrapper: MemoryRouterProvider
        });
        expect(screen.getByTestId("movie-list")).toBeInTheDocument();
    });
});