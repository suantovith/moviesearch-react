/**
 * @jest-environment jsdom
 */

import "@jest/globals";
import "@testing-library/jest-dom";
import { render, screen } from "@testing-library/react";
import { MemoryRouterProvider } from "next-router-mock/MemoryRouterProvider";
import SearchBox from "@/components/SearchBox";

describe("Renders movie searchbox app", () => {
    it("should render text input", () => {
        render(<SearchBox />, {
            wrapper: MemoryRouterProvider
        });
        expect(screen.getByTestId("searchbox")).toBeInTheDocument();
    });
});