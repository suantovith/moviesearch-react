/**
 * @jest-environment jsdom
 */

import "@jest/globals";
import "@testing-library/jest-dom";
import { render, screen } from "@testing-library/react";
import ThemeSwitcher from "@/components/ThemeSwitcher";

describe("Renders theme switcher for app", () => {
    it("should render switch", () => {
        render(<ThemeSwitcher />);
        expect(screen.getByTestId("theme-switch")).toBeInTheDocument();
    });
});