/**
 * @jest-environment jsdom
 */

import "@jest/globals";
import "@testing-library/jest-dom";
import { render, screen } from "@testing-library/react";
import { MemoryRouterProvider } from "next-router-mock/MemoryRouterProvider";
import Default from "@/layouts/Default";

describe("Renders default page layout", () => {
    it("should render main tag", () => {
        const prop: JSX.Element[] =  [<div key={0}>0</div>];
        render(<Default children={prop} />, {
            wrapper: MemoryRouterProvider
        });
        expect(screen.getByTestId("main")).toBeInTheDocument();
    });
});