import { describe, it, expect } from "@jest/globals";
import prepareString from "@/lib/prepareString";

describe("Preparing string to match moviedb format", () => {
    it("should return escaped, normalized and cleaned string",  () => {
        const data = prepareString("witcher test!@#");
        expect(data).toBe("witcher%20test%21@%23");
    });
});