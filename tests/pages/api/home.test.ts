import { describe, it, expect } from "@jest/globals";
import { getPopulars } from "@/pages/api/home";

describe("get popular movies data json by name", () => {
    it("should return popular movies by name", async () => {
        const data = await getPopulars();
        expect(data.length).toBeGreaterThan(0);
    });
});