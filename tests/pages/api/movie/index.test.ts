import { describe, it, expect } from "@jest/globals";
import { getMoviesByName } from "@/pages/api/movie/";

describe("get multiple movies data json by name", () => {
    it("should return movies by name", async () => {
        const data = await getMoviesByName("witcher");
        expect(data.length).toBeGreaterThan(0);
    });
});