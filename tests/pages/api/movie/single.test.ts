import { describe, it, expect } from "@jest/globals";
import { getMovieById } from "@/pages/api/movie/single";

describe("get movie data json by id", () => {
    it("should return movie by id", async () => {
        const data = await getMovieById(603692);
        expect(data.title).toBe("John Wick: Chapter 4");
    });
});