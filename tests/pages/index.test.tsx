/**
 * @jest-environment jsdom
 */
import { type Movie } from "@/lib/types";
import "@jest/globals";
import "@testing-library/jest-dom";
import { render, screen } from "@testing-library/react";
import { MemoryRouterProvider } from "next-router-mock/MemoryRouterProvider";
import Home from "@/pages";

describe("Rendered home page", () => {
    it("should return home hero section", () => {
        const movie: Movie = {
            id: 0,
            title: "Test",
            original_title: "_TEST_",
            overview: "TEST TEST TEST",
            poster_path: "",
            vote_average: "5.2"
        }
        render(<Home list={[movie]} />, {
            wrapper: MemoryRouterProvider
        });
        expect(screen.getByTestId("hero")).toBeInTheDocument();
    });
    it("should return home populars section", () => {
        const movie: Movie = {
            id: 0,
            title: "Test",
            original_title: "_TEST_",
            overview: "TEST TEST TEST",
            poster_path: "",
            vote_average: "5.2"
        }
        render(<Home list={[movie]} />, {
            wrapper: MemoryRouterProvider
        });
        expect(screen.getByTestId("populars")).toBeInTheDocument();
    });
});