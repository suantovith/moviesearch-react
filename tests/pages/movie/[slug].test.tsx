/**
 * @jest-environment jsdom
 */
import { type Movie } from "@/lib/types";
import configureStore, { type MockStoreCreator, type MockStoreEnhanced } from 'redux-mock-store';
import "@jest/globals";
import "@testing-library/jest-dom";
import { render, screen } from "@testing-library/react";
import { Provider } from 'react-redux';
import Single from "@/pages/movie/\[slug\]";

describe("Rendered single movie page", () => {
    it("should render movie image", () => {
        const initialState = {
            favorites: {
                list: []
            }
        };
        const mockStore: MockStoreCreator<any> = configureStore();
        const store: MockStoreEnhanced<any> = mockStore(initialState);

        const movie: Movie = {
            id: 0,
            title: "Test",
            original_title: "_TEST_",
            overview: "TEST TEST TEST",
            poster_path: "",
            vote_average: "5.2"
        }
        const prop = movie;

        render(
            <Provider store={store}>
                <Single item={prop} />
            </Provider>
        );
        expect(screen.getByTestId("poster")).toBeInTheDocument();
    });
});